import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, OneHotEncoder, LabelEncoder
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score, confusion_matrix, classification_report
from sklearn import set_config
import matplotlib.pyplot as plt
import seaborn as sns

set_config(display='diagram')

# Montează Google Drive
from google.colab import drive
drive.mount('/content/drive')

# Define path
file_path = "/content/drive/MyDrive/kddcup.data_10_percent.csv"

# Define column names
column_names = ["duration", "protocol_type", "service", "flag", "src_bytes",
                "dst_bytes", "land", "wrong_fragment", "urgent", "hot",
                "num_failed_logins", "logged_in", "num_compromised",
                "root_shell", "su_attempted", "num_root", "num_file_creations",
                "num_shells", "num_access_files", "num_outbound_cmds",
                "is_host_login", "is_guest_login", "count", "srv_count",
                "serror_rate", "srv_serror_rate", "rerror_rate", "srv_rerror_rate",
                "same_srv_rate", "diff_srv_rate", "srv_diff_host_rate",
                "dst_host_count", "dst_host_srv_count", "dst_host_same_srv_rate",
                "dst_host_diff_srv_rate", "dst_host_same_src_port_rate",
                "dst_host_srv_diff_host_rate", "dst_host_serror_rate",
                "dst_host_srv_serror_rate", "dst_host_rerror_rate",
                "dst_host_srv_rerror_rate", "label"]

# Încarcă setul de date într-un DataFrame Pandas
data = pd.read_csv(file_path, names=column_names, header=None)

# Primele câteva rânduri ale DataFrame-ului
print(data.head())

# Statistici sumare ale setului de date
print(data.describe())

# Verificarea tipurilor de date ale fiecărei coloane
print(data.dtypes)

# Selectarea unui subset aleatoriu din date (de exemplu, 10%)
data_subset = data.sample(frac=0.1, random_state=42)

# Encodarea coloanelor categorice și separarea caracteristicilor de etichete
categorical_features = ["protocol_type", "service", "flag"]
numerical_features = [col for col in data.columns if col not in categorical_features + ["label"]]

preprocessor = ColumnTransformer(
    transformers=[
        ('num', StandardScaler(), numerical_features),
        ('cat', OneHotEncoder(handle_unknown='ignore'), categorical_features)])

# Împărțirea datelor în caracteristici și etichete
X = data_subset.drop(columns=["label"])
y = data_subset["label"]

# Aplicarea preprocesării
X = preprocessor.fit_transform(X)

# Conversia etichetelor reale la tip numeric
le = LabelEncoder()
y_numeric = le.fit_transform(y)

# Determinarea numărului optim de clustere folosind metoda "Elbow"
sse = []
for k in range(1, 11):
    kmeans = KMeans(n_clusters=k, random_state=42)
    kmeans.fit(X)
    sse.append(kmeans.inertia_)

plt.figure(figsize=(10, 6))
plt.plot(range(1, 11), sse, marker='o')
plt.xlabel('Number of clusters')
plt.ylabel('SSE')
plt.title('Elbow Method For Optimal k')
plt.show()

# Construirea modelului K-means cu un număr de clustere ales (de exemplu, 4)
optimal_clusters = 4
kmeans = KMeans(n_clusters=optimal_clusters, random_state=42)
kmeans.fit(X)

# Adăugarea etichetelor de clustere la date
data_subset['cluster'] = kmeans.labels_

# Vizualizarea distribuției clusterelor
plt.figure(figsize=(10, 6))
sns.countplot(x='cluster', data=data_subset)
plt.title('Cluster Distribution')
plt.show()

# Calcularea scorului siluetei pentru evaluarea performanței clusteringului
silhouette_avg = silhouette_score(X, kmeans.labels_)
print("Silhouette Score: ", silhouette_avg)

# Vizualizarea clusterelor pe primele două componente principale
from sklearn.decomposition import PCA

pca = PCA(n_components=2)
X_pca = pca.fit_transform(X)

plt.figure(figsize=(10, 6))
sns.scatterplot(x=X_pca[:, 0], y=X_pca[:, 1], hue=kmeans.labels_, palette='viridis')
plt.title('Clusters Visualization on PCA Components')
plt.show()

# Funcție pentru afișarea matricei de confuzie și a raportului de clasificare
def display_confusion_matrix_and_classification_report(true_labels, cluster_labels, class_names):
    # Matricea de confuzie
    conf_matrix = confusion_matrix(true_labels, cluster_labels)
    plt.figure(figsize=(14, 14))
    ax = sns.heatmap(conf_matrix, annot=True, fmt='d', cmap='Blues', xticklabels=class_names, yticklabels=class_names)
    plt.ylabel('Actual')
    plt.xlabel('Cluster')
    plt.title('Confusion Matrix')

    # Adăugarea barei de culoare pe orizontală sub matricea de confuzie
    cbar = plt.colorbar(ax.get_children()[0], orientation='horizontal', fraction=0.05, pad=0.15)
    cbar.set_label('Scale')

    plt.show()

    # Generarea raportului de clasificare
    report = classification_report(true_labels, cluster_labels, zero_division=1, target_names=class_names, output_dict=True)
    report_df = pd.DataFrame(report).transpose()

    # Inserarea unui rând gol între 'accuracy' și valorile 'avg.'
    report_df.loc[' '] = ['', '', '', '']  # Rând gol
    report_df = report_df.sort_index()  # Re-sortare pentru a păstra ordinea corectă

    # Afișarea raportului de clasificare într-un format tabelar
    print(report_df)

# Afișarea matricei de confuzie și a raportului de clasificare
class_names = le.classes_
display_confusion_matrix_and_classification_report(y_numeric, kmeans.labels_, class_names)