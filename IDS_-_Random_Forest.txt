import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.preprocessing import StandardScaler, OneHotEncoder
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report, confusion_matrix
import matplotlib.pyplot as plt
import seaborn as sns

# Montează Google Drive
from google.colab import drive
drive.mount('/content/drive')

# Define path
file_path = "/content/drive/MyDrive/kddcup.data_10_percent.csv"

# Define column names
column_names = ["duration", "protocol_type", "service", "flag", "src_bytes",
                "dst_bytes", "land", "wrong_fragment", "urgent", "hot",
                "num_failed_logins", "logged_in", "num_compromised",
                "root_shell", "su_attempted", "num_root", "num_file_creations",
                "num_shells", "num_access_files", "num_outbound_cmds",
                "is_host_login", "is_guest_login", "count", "srv_count",
                "serror_rate", "srv_serror_rate", "rerror_rate", "srv_rerror_rate",
                "same_srv_rate", "diff_srv_rate", "srv_diff_host_rate",
                "dst_host_count", "dst_host_srv_count", "dst_host_same_srv_rate",
                "dst_host_diff_srv_rate", "dst_host_same_src_port_rate",
                "dst_host_srv_diff_host_rate", "dst_host_serror_rate",
                "dst_host_srv_serror_rate", "dst_host_rerror_rate",
                "dst_host_srv_rerror_rate", "label"]

# Încarcă setul de date într-un DataFrame Pandas
data = pd.read_csv(file_path, names=column_names, header=None)

# Primele câteva rânduri ale DataFrame-ului
print(data.head())

# Statistici sumare ale setului de date
print(data.describe())

# Verificarea tipurilor de date ale fiecărei coloane
print(data.dtypes)

from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import OneHotEncoder, StandardScaler
from sklearn.impute import SimpleImputer
from sklearn.pipeline import Pipeline
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split

# Definirea caracteristicilor și etichetelor
features = data.iloc[:, :-1]  # Exclude ultima coloană care este 'label'
labels = data['label']

# Verificarea valorilor unice din etichete înainte de preprocesare
print("Unique labels before factorization:", labels.unique())
print("Label distribution before factorization:\n", labels.value_counts())

# Convertirea etichetelor în valori numerice
labels, uniques = pd.factorize(labels)

# Verificarea valorilor unice din etichete după factorization
print("Unique labels after factorization:", np.unique(labels))
print("Label distribution after factorization:\n", pd.Series(labels).value_counts())

# Identificarea coloanelor categorice și numerice
categorical_columns = features.select_dtypes(include=['object']).columns.tolist()
numeric_columns = features.select_dtypes(include=['int64', 'float64']).columns.tolist()

# Crearea transformatorului pentru coloanele numerice
numeric_transformer = Pipeline(steps=[
    ('imputer', SimpleImputer(strategy='mean')),
    ('scaler', StandardScaler())])

# Crearea transformatorului pentru coloanele categorice
categorical_transformer = Pipeline(steps=[
    ('imputer', SimpleImputer(strategy='constant', fill_value='missing')),
    ('onehot', OneHotEncoder(handle_unknown='ignore'))])

# Combinarea transformatorilor într-un preprocesor
preprocessor = ColumnTransformer(
    transformers=[
        ('num', numeric_transformer, numeric_columns),
        ('cat', categorical_transformer, categorical_columns)])

# Împărțirea datelor în seturi de antrenament și test
X_train, X_test, y_train, y_test = train_test_split(features, labels, test_size=0.3, random_state=42)

# Verificarea distribuției etichetelor în seturile de antrenament și test
print("Label distribution in y_train:\n", pd.Series(y_train).value_counts())
print("Label distribution in y_test:\n", pd.Series(y_test).value_counts())

# Aplicarea preprocesării asupra datelor de antrenament
X_train = preprocessor.fit_transform(X_train)

# Aplicarea aceleași preprocesări asupra datelor de test
X_test = preprocessor.transform(X_test)

from sklearn.ensemble import RandomForestClassifier

# Crearea și antrenarea modelului Random Forest
clf = RandomForestClassifier(n_estimators=100, random_state=42)
clf.fit(X_train, y_train)

from sklearn.metrics import accuracy_score, classification_report

# Prezicerea etichetelor pentru setul de test
y_pred = clf.predict(X_test)

# Evaluarea performanței modelului
print("Accuracy:", accuracy_score(y_test, y_pred))
print("Classification Report:\n", classification_report(y_test, y_pred))

# Verificarea etichetelor din y_test și y_pred
print("Unique values in y_test:", np.unique(y_test))
print("Unique values in y_pred:", np.unique(y_pred))

import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.metrics import confusion_matrix

# Calcularea matricei de confuzie
cm = confusion_matrix(y_test, y_pred)

# Obținerea etichetelor unice din setul de date
labels = sorted(data['label'].unique())

# Afișarea matricei de confuzie folosind seaborn
plt.figure(figsize=(20, 20))  # Ajustarea dimensiunii figurii pentru a se potrivi mai bine etichetele
ax = sns.heatmap(cm, annot=True, fmt='d', cmap='Blues', xticklabels=labels, yticklabels=labels, cbar=False)
plt.xlabel('Predicted')
plt.ylabel('Actual')
plt.title('Confusion Matrix')
plt.xticks(rotation=90)  # Rotirea etichetelor de pe axa X pentru vizibilitate mai bună
plt.yticks(rotation=0)  # Asigurarea că etichetele de pe axa Y sunt orizontale

# Adăugarea barei de culoare pe orizontală sub matricea de confuzie
cbar = plt.colorbar(ax.get_children()[0], orientation='horizontal', fraction=0.05, pad=0.10)  # Ajustează pad și fraction pentru a apropia bara
cbar.set_label('Scale')

plt.show()