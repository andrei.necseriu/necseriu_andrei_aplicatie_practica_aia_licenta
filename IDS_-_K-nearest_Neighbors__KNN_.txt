import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.preprocessing import StandardScaler, OneHotEncoder
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn import set_config
import matplotlib.pyplot as plt
import seaborn as sns

set_config(display='diagram')

# Montează Google Drive
from google.colab import drive
drive.mount('/content/drive')

# Define path
file_path = "/content/drive/MyDrive/kddcup.data_10_percent.csv"

# Define column names
column_names = ["duration", "protocol_type", "service", "flag", "src_bytes",
                "dst_bytes", "land", "wrong_fragment", "urgent", "hot",
                "num_failed_logins", "logged_in", "num_compromised",
                "root_shell", "su_attempted", "num_root", "num_file_creations",
                "num_shells", "num_access_files", "num_outbound_cmds",
                "is_host_login", "is_guest_login", "count", "srv_count",
                "serror_rate", "srv_serror_rate", "rerror_rate", "srv_rerror_rate",
                "same_srv_rate", "diff_srv_rate", "srv_diff_host_rate",
                "dst_host_count", "dst_host_srv_count", "dst_host_same_srv_rate",
                "dst_host_diff_srv_rate", "dst_host_same_src_port_rate",
                "dst_host_srv_diff_host_rate", "dst_host_serror_rate",
                "dst_host_srv_serror_rate", "dst_host_rerror_rate",
                "dst_host_srv_rerror_rate", "label"]

# Încarcă setul de date într-un DataFrame Pandas
data = pd.read_csv(file_path, names=column_names, header=None)

# Primele câteva rânduri ale DataFrame-ului
print(data.head())

# Statistici sumare ale setului de date
print(data.describe())

# Verificarea tipurilor de date ale fiecărei coloane
print(data.dtypes)

# Encodarea coloanelor categorice și separarea caracteristicilor de etichete
categorical_features = ["protocol_type", "service", "flag"]
numerical_features = [col for col in data.columns if col not in categorical_features + ["label"]]

preprocessor = ColumnTransformer(
    transformers=[
        ('num', StandardScaler(), numerical_features),
        ('cat', OneHotEncoder(handle_unknown='ignore'), categorical_features)])

# Împărțirea datelor în caracteristici și etichete
X = data.drop(columns=["label"])
y = data["label"]

# Împărțirea datelor în seturi de antrenament și testare
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Aplicarea preprocesării
X_train = preprocessor.fit_transform(X_train)
X_test = preprocessor.transform(X_test)

# Construirea modelului KNN
knn = KNeighborsClassifier(n_neighbors=5)
knn.fit(X_train, y_train)

# Importarea bibliotecilor necesare pentru vizualizare
import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score

# Prezicerea etichetelor pentru setul de testare
y_pred = knn.predict(X_test)

# Evaluarea performanței modelului
print("Accuracy:", accuracy_score(y_test, y_pred))
print("Classification Report:\n", classification_report(y_test, y_pred))

# Matricea de confuzie
conf_matrix = confusion_matrix(y_test, y_pred)
plt.figure(figsize=(14, 14))
ax = sns.heatmap(conf_matrix, annot=True, fmt='d', cmap='Blues')
plt.ylabel('Actual')
plt.xlabel('Predicted')
plt.title('Confusion Matrix')

# Adăugarea barei de culoare pe orizontală sub matricea de confuzie
cbar = plt.colorbar(ax.get_children()[0], orientation='horizontal', fraction=0.05, pad=0.07)
cbar.set_label('Scale')

plt.show()

# Generarea raportului de clasificare
report = classification_report(y_test, y_pred, zero_division=1, output_dict=True)
report_df = pd.DataFrame(report).transpose()

# Inserarea unui rând gol între 'accuracy' și valorile 'avg.'
report_df.loc[' '] = ['', '', '', '']  # Rând gol
report_df = report_df.sort_index()  # Re-sortare pentru a păstra ordinea corectă

# Afișarea raportului de clasificare într-un format tabelar
print(report_df)

