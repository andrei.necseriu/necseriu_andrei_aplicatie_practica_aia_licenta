# Importul bibliotecilor necesare
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import classification_report, accuracy_score, confusion_matrix
import matplotlib.pyplot as plt
import seaborn as sns
import joblib
import tensorflow as tf
from tensorflow import keras

# Montează Google Drive
from google.colab import drive
drive.mount('/content/drive')

# Define path
file_path = "/content/drive/MyDrive/kddcup.data_10_percent.csv"

# Define column names
column_names = ["duration", "protocol_type", "service", "flag", "src_bytes",
                "dst_bytes", "land", "wrong_fragment", "urgent", "hot",
                "num_failed_logins", "logged_in", "num_compromised",
                "root_shell", "su_attempted", "num_root", "num_file_creations",
                "num_shells", "num_access_files", "num_outbound_cmds",
                "is_host_login", "is_guest_login", "count", "srv_count",
                "serror_rate", "srv_serror_rate", "rerror_rate", "srv_rerror_rate",
                "same_srv_rate", "diff_srv_rate", "srv_diff_host_rate",
                "dst_host_count", "dst_host_srv_count", "dst_host_same_srv_rate",
                "dst_host_diff_srv_rate", "dst_host_same_src_port_rate",
                "dst_host_srv_diff_host_rate", "dst_host_serror_rate",
                "dst_host_srv_serror_rate", "dst_host_rerror_rate",
                "dst_host_srv_rerror_rate", "label"]

# Încarcă setul de date într-un DataFrame Pandas
data = pd.read_csv(file_path, names=column_names, header=None)

# Primele câteva rânduri ale DataFrame-ului
print(data.head())

# Statistici sumare ale setului de date
print(data.describe())

# Verificarea tipurilor de date ale fiecărei coloane
print(data.dtypes)

# Identifică coloanele categorice
categorical_cols = data.select_dtypes(include=['object']).columns

# Verificare coloane categorice
print("Coloane categorice:", categorical_cols)

# Encode categorical columns
label_encoder = LabelEncoder()
for col in categorical_cols:
    data[col] = label_encoder.fit_transform(data[col])

# Split features and target
X = data.drop('label', axis=1)
y = data['label']

# Verificare dimensiuni X și y
print("Dimensiuni X:", X.shape)
print("Dimensiuni y:", y.shape)

# Train-test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Initialize the model
dt_classifier = DecisionTreeClassifier()

# Train the model
dt_classifier.fit(X_train, y_train)

# Predict on test data
y_pred = dt_classifier.predict(X_test)

# Evaluate the model
print("Accuracy:", accuracy_score(y_test, y_pred))
print("Classification Report:\n", classification_report(y_test, y_pred))

import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.metrics import confusion_matrix

# Calcularea matricei de confuzie
conf_matrix = confusion_matrix(y_test, y_pred)

# Vizualizarea matricei de confuzie
plt.figure(figsize=(20, 14))  # Ajustează dimensiunea figurii pentru a lăsa mai mult spațiu în partea de jos
ax = sns.heatmap(conf_matrix, annot=True, fmt='d', cmap='Blues', xticklabels=label_encoder.classes_, yticklabels=label_encoder.classes_, cbar=False)
plt.xlabel('Predicted')
plt.ylabel('Actual')
plt.title('Confusion Matrix')

# Adăugarea barei de culoare pe orizontală sub matricea de confuzie
cbar = plt.colorbar(ax.get_children()[0], orientation='horizontal', fraction=0.046, pad=0.15)  # Ajustează pad pentru a adăuga mai mult spațiu
cbar.set_label('Scale')

plt.show()