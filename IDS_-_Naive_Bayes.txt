import pandas as pd
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix
from sklearn.impute import SimpleImputer
import matplotlib.pyplot as plt
import seaborn as sns

# Montează Google Drive
from google.colab import drive
drive.mount('/content/drive')

# Define path
file_path = "/content/drive/MyDrive/kddcup.data_10_percent.csv"

# Define column names
column_names = ["duration", "protocol_type", "service", "flag", "src_bytes",
                "dst_bytes", "land", "wrong_fragment", "urgent", "hot",
                "num_failed_logins", "logged_in", "num_compromised",
                "root_shell", "su_attempted", "num_root", "num_file_creations",
                "num_shells", "num_access_files", "num_outbound_cmds",
                "is_host_login", "is_guest_login", "count", "srv_count",
                "serror_rate", "srv_serror_rate", "rerror_rate", "srv_rerror_rate",
                "same_srv_rate", "diff_srv_rate", "srv_diff_host_rate",
                "dst_host_count", "dst_host_srv_count", "dst_host_same_srv_rate",
                "dst_host_diff_srv_rate", "dst_host_same_src_port_rate",
                "dst_host_srv_diff_host_rate", "dst_host_serror_rate",
                "dst_host_srv_serror_rate", "dst_host_rerror_rate",
                "dst_host_srv_rerror_rate", "label"]

# Încarcă setul de date într-un DataFrame Pandas
data = pd.read_csv(file_path, names=column_names, header=None)

# Primele câteva rânduri ale DataFrame-ului
print(data.head())

# Statistici sumare ale setului de date
print(data.describe())

# Verificarea tipurilor de date ale fiecărei coloane
print(data.dtypes)

# Preprocesarea datelor
# Codificarea etichetelor categorice
label_encoders = {}
categorical_columns = ['protocol_type', 'service', 'flag']

for column in categorical_columns:
    label_encoders[column] = LabelEncoder()
    data[column] = label_encoders[column].fit_transform(data[column])

# Împărțirea setului de date în caracteristici (X) și etichete (y)
X = data.drop(['label'], axis=1)
y = data['label']

# Imputarea valorilor lipsă
imputer = SimpleImputer(strategy='mean')
X = imputer.fit_transform(X)

# Standardizarea caracteristicilor
scaler = StandardScaler()
X_scaled = scaler.fit_transform(X)

# Împărțirea datelor în seturi de antrenament și test
X_train, X_test, y_train, y_test = train_test_split(X_scaled, y, test_size=0.3, random_state=42)

# Verificarea valorilor NaN
print(f'Number of NaN values in X_train: {pd.DataFrame(X_train).isna().sum().sum()}')
print(f'Number of NaN values in X_test: {pd.DataFrame(X_test).isna().sum().sum()}')

# Verificarea distribuției claselor în setul de date original
plt.figure(figsize=(10, 6))
data['label'].value_counts().plot(kind='bar')
plt.title('Distribuția claselor în setul de date original')
plt.xlabel('Clase')
plt.ylabel('Numărul de exemple')
plt.show()

# Distribuția claselor în setul de antrenament
plt.figure(figsize=(10, 6))
pd.Series(y_train).value_counts().plot(kind='bar')
plt.title('Distribuția claselor în setul de antrenament')
plt.xlabel('Clase')
plt.ylabel('Numărul de exemple')
plt.show()

# Distribuția claselor în setul de test
plt.figure(figsize=(10, 6))
pd.Series(y_test).value_counts().plot(kind='bar')
plt.title('Distribuția claselor în setul de test')
plt.xlabel('Clase')
plt.ylabel('Numărul de exemple')
plt.show()

# Crearea și antrenarea modelului Naive Bayes
model = GaussianNB()
model.fit(X_train, y_train)

# Evaluarea modelului
y_pred = model.predict(X_test)
accuracy = accuracy_score(y_test, y_pred)
print(f'Accuracy: {accuracy:.2f}')
print('Classification Report:')
print(classification_report(y_test, y_pred, zero_division=1))

# Matricea de confuzie
conf_matrix = confusion_matrix(y_test, y_pred)

# Etichetele claselor
classes = y_test.unique()
classes.sort()

plt.figure(figsize=(20, 15))
sns.heatmap(conf_matrix, annot=True, fmt='d', cmap='Blues', xticklabels=classes, yticklabels=classes, cbar_kws={'orientation': 'horizontal'})
plt.title('Confusion Matrix')
plt.xlabel('Predicted Label')
plt.ylabel('True Label')
plt.show()
